package com.rpgfarm.crongcore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.String;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.rpgfarm.crongcore.GetInfo;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.ExpressionType;

public class main
  extends JavaPlugin
  implements Listener, Runnable
{
  public static FileConfiguration config;
  public Thread serverThread;
  public static String latestCommand = "";
  String ip;
  
  public static String m(String message)
  {
    return ChatColor.translateAlternateColorCodes('&', message);
  }
  
  public void onEnable()
  {
	this.ip = "127.0.0.1";
	System.out.println("[CrongCore] Crong Core는 다음 아이피에서 접근되는 모든 요청을 허용합니다: " + this.ip);
    config = getConfig();
    config.addDefault("lastcommand", "CrongCore의 마지막 명령어 전송 요청이 표시됩니다.");
    config.addDefault("setting.port", Integer.valueOf(3208));
    config.options().copyDefaults(true);
    saveConfig();
    saveDefaultConfig();
    Bukkit.getConsoleSender().sendMessage("Crong Core가 로드중입니다. Powered by Baw Service");
    Bukkit.getConsoleSender().sendMessage("해당 플러그인은 크롱 클라우드에서 구동되는 크롱 서버에게만 사용이 허가됩니다.");
    Bukkit.getConsoleSender().sendMessage("BungeeCord 서비스를 등록합니다.");
    getServer().getPluginManager().registerEvents(this, this);
    Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

    Bukkit.getConsoleSender().sendMessage("Skript 서비스를 등록합니다.");
    Skript.registerAddon(this);
    Skript.registerExpression(GetInfo.class, String.class, ExpressionType.PROPERTY, new String[] { "info of %string%" });

    Bukkit.getConsoleSender().sendMessage("Socket 서비스를 등록합니다.");
    final main mg = this;
    this.serverThread = new Thread(new Runnable()
    {
      public void run()
      {
        ServerSocket server = null;
        try
        {
          server = new ServerSocket(main.config.getInt("setting.port"));
          for (;;)
          {
            Socket client = server.accept();
        	if (client.getInetAddress().getHostName().equals(main.this.ip)) {
	            TSocket echo = new TSocket(client, mg);
	            echo.start();
	        } else {
	        	System.out.println("[CrongCore] Baw Account 요청 IP 불일치: " + client.getInetAddress().getHostName());
	        	client.close();
	        }
          }
        }
        catch (IOException e)
        {
          System.out.println("[CrongCore] CrongCore의 Input/Output 오류입니다.");
        }
      }
    });
    Bukkit.getPluginManager().registerEvents(this, this);
    this.serverThread.start();
  }
  
  public void onDisable()
  {
	System.out.println("[CrongCore] CrongCore가 비활성화중입니다. 사용해주셔서 감사합니다.");
  }

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("이 명령어는 콘솔에서 실행할 수 없습니다.");
      return false;
    }
    Player p = (Player)sender;
    if ((commandLabel.equalsIgnoreCase("CCWARP")) && 
      (args.length == 1))
    {
      ByteArrayDataOutput out = ByteStreams.newDataOutput();
      out.writeUTF("Connect");
      out.writeUTF(args[0].toString());
      p.sendPluginMessage(this, "BungeeCord", out.toByteArray());
      return true;
    }
    return true;
  }
  
  public void run()
  {
    ServerSocket server = null;
    try
    {
      server = new ServerSocket(config.getInt("setting.port"));
      for (;;)
      {
    	  Socket client = server.accept();
    	  if (client.getInetAddress().getHostName().equals(this.ip))
    	  {
    		  TSocket echo = new TSocket(client, this);
    		  echo.start();
    	  }
    	  else
    	  {
    		  client.close();
    		  System.out.println("[CrongCore] CrongCore 요청 IP 불일치: " + client.getInetAddress().getHostName());
    	  }
      }
    }
    catch (IOException e)
    {
       System.out.println("[CrongCore] CrongCore의 Input/Output 오류입니다.");
    }
  }
  
  
  
  
public String getOpenStreamHTML(String urlToRead)
	{
		String result = "";
		try
		{
			URL url = new URL(urlToRead);
			InputStreamReader is = new InputStreamReader(url.openStream(), "UTF-8");
			int ch;
			while((ch = is.read()) != -1) 
				result = (new StringBuilder(String.valueOf(result))).append((char)ch).toString();
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return result;
		}



public void logsave(String fla, String text)
  {
    File f = new File("plugins\\CrongCore\\log.log");
    if (!f.exists())
    {
      new File("plugins\\CrongCore\\").mkdirs();
      try
      {
        f.createNewFile();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm");
    Date date = new Date();
    try
    {
      File file = new File("plugins\\CrongCore\\log.log");
      FileReader fileReader = new FileReader(file);
      BufferedReader in = new BufferedReader(
        new InputStreamReader(
        new FileInputStream("plugins\\CrongCore\\log.log"), "UTF8"));
      StringBuffer stringBuffer = new StringBuffer();
      String line;
      while ((line = in.readLine()) != null)
      {
        stringBuffer.append(line);
        stringBuffer.append("\n");
      }
      fileReader.close();
      String frmtdDate = dateFormat.format(date);
      try
      {
        PrintWriter writer = new PrintWriter("plugins\\CrongCore\\log.log", "UTF-8");
        writer.println(stringBuffer.toString() + "[" + frmtdDate + "] " + text + fla);
        writer.close();
      }
      catch (FileNotFoundException localFileNotFoundException) {}catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
      in.close();
      return;
    }
    catch (IOException localIOException1) {}
  }
}
