package com.rpgfarm.crongcore;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.lang.String;

import javax.annotation.Nullable;

import org.bukkit.event.Event;

import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;

public class GetInfo extends SimpleExpression<String> {
	
    private Expression<String> SVPORT;
    
	@Override
	public Class<? extends String> getReturnType() {
	    return String.class;
	}
 
    @Override
    public boolean isSingle() {
        return true;
    }

    @SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean arg2, SkriptParser.ParseResult arg3)
    {
      this.SVPORT = (Expression<String>) expr[0];
      return true;
    }
 
    @Override
    public String toString(@Nullable Event event, boolean debug) {
        return "null";
    }
 
    protected String[] get(Event event) {
		int i = Integer.parseInt(SVPORT.getSingle(event));
		System.out.println(i);
    	try {
    		@SuppressWarnings("resource")
			Socket sock = new Socket("localhost", i);
    		 
    		DataOutputStream out = new DataOutputStream(sock.getOutputStream());
    		DataInputStream in = new DataInputStream(sock.getInputStream());
    		 
    		out.write(0xFE);
    		 
    		int b;
    		StringBuffer string = new StringBuffer();
    		while ((b = in.read()) != -1) {
    		if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
	    		// Not sure what use the two characters are so I omit them
	    		string.append((char) b);
    		}
    	}
    	String str2 = new String(string);
    	String str = new String(str2.getBytes("ms949"), "UTF-8");

    	System.out.println(str);
		String[] data = str.toString().split("§");
		String serverMotd = data[0];
		int onlinePlayers = Integer.parseInt(data[1]);
		int maxPlayers = Integer.parseInt(data[2]);
		 
	    System.out.println(serverMotd + onlinePlayers + maxPlayers);
	    return new String[] { String.format("MOTD: \"%s\"\nOnline Players: %d/%d", serverMotd, onlinePlayers, maxPlayers) };
		} catch (UnknownHostException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
		} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
		}
		return null;
    }
}
