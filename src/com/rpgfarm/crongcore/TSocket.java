package com.rpgfarm.crongcore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import org.bukkit.Bukkit;

public class TSocket
  extends Thread
{
  protected Socket socket;
  private final main plugin;
  
  public TSocket(Socket socket, main main)
  {
    this.socket = socket;
    this.plugin = main;
  }
  
  public void run()
  {
    try
    {
      InputStream is = this.socket.getInputStream();
      InputStreamReader isr = new InputStreamReader(is);
      OutputStream os = this.socket.getOutputStream();
      OutputStreamWriter osr = new OutputStreamWriter(os);
      BufferedReader br = new BufferedReader(isr);
      String command = br.readLine();
      if (command.split(";")[0].equals("command")) {
          String cmd = command.split(";")[1];
          String str = cmd;
          this.plugin.getConfig().set("lastcommand", str);
          this.plugin.saveConfig();
          Bukkit.dispatchCommand(Bukkit.getConsoleSender(), str);
          this.plugin.logsave(str, "명령어 실행 요청을 받았습니다: ");
          System.out.println("[CrongCore] CrongSub에서 명령어 실행 요청을 받았습니다.");
          sendPacket(osr, "성공");
        }
    }catch (Exception e) {
		// TODO: handle exception
	}
  }
  
  private void sendPacket(OutputStreamWriter osr, String msg)
  {
    BufferedWriter bw = new BufferedWriter(osr);
    PrintWriter pw = new PrintWriter(bw);
    pw.println(msg);
    pw.flush();
  }
}
